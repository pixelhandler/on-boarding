/* eslint-env node */
'use strict';

const multer = require('multer')();

module.exports = function(app) {
  const express = require('express');
  let registerCommandRouter = express.Router();

  registerCommandRouter.post('/', multer.fields([]), function(req, res) {
    console.log('register-command post body: ', JSON.stringify(req.body));
    const props = Object.keys(req.body);
    if (props.length === 2 && props.includes('username') && props.includes('password')) {
      res.status(201).end();
    } else {
      res.status(400).end();
    }
  });

  // app.use('/api/register-command', require('body-parser').urlencoded({ extended: true }));
  app.use('/api/register-command', registerCommandRouter);
};

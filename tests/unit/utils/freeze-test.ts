import freeze from 'on-boarding/utils/freeze';
import { module, test } from 'qunit';
import config from 'on-boarding/config/environment';

module('Unit | Utility | freeze');

class Thing {
  _name: string;

  constructor() {
    this._name = 'Thing';
    freeze(this);
  }
  get name() {
    return this._name;
  }
}

test('it freezes an object', function(assert: any) {
  let frozen: Thing = new Thing();
  assert.ok(frozen);
  assert.throws(function (): void {
    frozen._name = 'cowboy';
  }, Error);
});

type _o_ = {
  nested: {
    name: string
  },
  name: string,
};

test('it freezes a nested object', function(assert: any) {
  const obj: _o_ = {
    nested: {
      name: 'Wyatt',
    },
    name: 'gambler',
  };
  freeze(obj);
  assert.throws(function (): void {
    obj.name = 'Sheriff Earp';
  }, Error);
  assert.throws(function (): void {
    obj.nested.name = 'sheriff';
  }, Error);
});

test('it freezes an array with an object', function(assert: any) {
  const arr: Array<_o_> = [{nested:{name:'Doc'},name:'cowboy'}];
  freeze(arr);
  assert.throws(function (): void {
    const y: _o_ = {nested:{name:'x'},name:'z'};
    arr.push(y);
  }, Error);
  assert.throws(function (): void {
    arr[0].name = 'Dr. Holiday';
  }, Error);
  assert.throws(function (): void {
    arr[0].nested.name = 'Dentist';
  }, Error);
});

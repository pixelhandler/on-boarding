import { moduleFor, test } from 'ember-qunit';

moduleFor('route:register', 'Unit | Route | register', {
  needs: ['service:register-command-repo']
});

test('it exists', function(assert: any) {
  let route = this.subject();
  assert.ok(route);
});

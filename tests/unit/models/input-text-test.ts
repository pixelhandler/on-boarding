import { module, test } from 'qunit';
import IInputElement from 'on-boarding/interfaces/input-element';
import IInputTextModel from 'on-boarding/interfaces/input-text-model';
import InputTextModel from 'on-boarding/models/input-text';

module('Unit | Model | input text');

test('instantiates', function (assert: any) {
  const props: IInputElement = {
    name: 'username',
    labelText: 'Username',
    type: 'text',
    value: '',
  }
  const model: IInputTextModel = new InputTextModel(props);
  assert.ok(!!model);
});

test('can clone an instance and change props', function (assert: any) {
  let props: IInputElement = {
    name: 'username',
    labelText: 'Username',
    type: 'text',
    value: '',
  }
  const model: InputTextModel = new InputTextModel(props);
  props = {
    value: 'pixelhandler',
  };
  const cloneOne: InputTextModel = InputTextModel.create(props, model);
  assert.ok(!!cloneOne);
  ['name', 'labelText', 'type'].forEach(propName => {
    assert.equal(cloneOne[propName], model[propName]);
  });
  assert.equal(cloneOne.value, 'pixelhandler');

  props = {
    labelText: 'User Name',
  };
  const cloneTwo: InputTextModel = InputTextModel.create(props, cloneOne);
  assert.ok(!!cloneTwo);
  ['name', 'type', 'value'].forEach(propName => {
    assert.equal(cloneTwo[propName], cloneOne[propName]);
  });
  assert.equal(cloneTwo.labelText, 'User Name');
});

test('is immutable', function (assert: any) {
  const props: IInputElement = {
    name: 'pasword',
    labelText: 'Password',
    type: 'password',
    value: 'secret',
  }
  const model: IInputTextModel = new InputTextModel(props);
  assert.throws(function (): void {
    model.name = 'cowboy';
  }, Error);
  assert.throws(function (): void {
    model.labelText = 'Cowboy';
  }, Error);
  assert.throws(function (): void {
    model.type = 'text';
  }, Error);
  assert.throws(function (): void {
    model.value = 'wyatt.earp';
  }, Error);
});

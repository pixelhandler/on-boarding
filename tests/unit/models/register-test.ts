import { module, test } from 'qunit';
import IRegisterModel from 'on-boarding/interfaces/register-model';
import RegisterModel from 'on-boarding/models/register';
import ICredentials from 'on-boarding/interfaces/credentials';
import IInputTextModel from 'on-boarding/interfaces/input-text-model';
import InputTextModel from 'on-boarding/models/input-text';

module('Unit | Model | register');

test('instantiates', function(assert: any) {
  const props: ICredentials = {
    username: '',
    password: '',
  };
  let model: IRegisterModel = new RegisterModel(props);
  assert.ok(!!model);
});

test('can clone an instance and change props', function (assert: any) {
  let props: ICredentials = {
    username: 'pixelhandler',
    password: 'changeme',
    confirmPassword: '',
  };
  const model: RegisterModel = new RegisterModel(props);
  props = {
    confirmPassword: 'changeme',
  };
  const cloneOne: RegisterModel = RegisterModel.create(props, model);
  assert.ok(!!cloneOne);
  ['username', 'password'].forEach(propName => {
    assert.equal(cloneOne[propName], model[propName]);
  });
  assert.equal(cloneOne.confirmPassword, 'changeme');

  props = {
    password: 'secret',
    confirmPassword: 'secret',
  };
  const cloneTwo: RegisterModel = RegisterModel.create(props, cloneOne);
  assert.ok(!!cloneTwo);
  assert.equal(cloneTwo.username, cloneOne.username);
  ['password', 'confirmPassword'].forEach(propName => {
    assert.equal(cloneTwo[propName], 'secret');
  });
});

test('is immutable', function (assert: any) {
  const props: ICredentials = {
    username: 'pixelhandler',
    password: 'changeme',
    confirmPassword: '',
  };
  const model: IRegisterModel = new RegisterModel(props);
  assert.throws(function (): void {
    model.username = 'cowboy';
  }, Error);
  assert.throws(function (): void {
    model.password = '123456';
  }, Error);
  assert.throws(function (): void {
    model.confirmPassword = 'secret';
  }, Error);
  assert.throws(function (): void {
    model.inputs.push(null);
  }, Error);
  assert.throws(function (): void {
    model.inputs[0] = null;
  }, Error);
  assert.throws(function (): void {
    model.input.username = null;
  }, Error);
});

test('toJSON method', function (assert: any) {
  const props: ICredentials = {
    username: 'pixelhandler',
    password: 'secret',
    confirmPassword: 'secret',
  };
  const model: RegisterModel = new RegisterModel(props);
  const json: ICredentials = model.toJSON();
  ['username', 'password'].forEach(propName => {
    assert.equal(json[propName], model[propName]);
  });
  assert.equal(json.confirmPassword, undefined);
});

test('creates inputs for username, password, confirmPassword', function (assert: any) {
  let props: ICredentials = {};
  let model: RegisterModel = new RegisterModel(props);
  const propNames: Array<string> = ['username', 'password', 'confirmPassword'];
  propNames.forEach((inputName: string): void => {
    assert.ok(model.input[inputName] instanceof InputTextModel);
  });

  props = {
    username: 'pixelhandler',
    password: 'secret',
    confirmPassword: 'secret',
  };
  model = RegisterModel.create(props, model);
  propNames.forEach((inputName: string): void => {
    assert.equal(model.input[inputName].value, props[inputName]);
  });
});

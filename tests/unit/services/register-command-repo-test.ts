import { moduleFor, test } from 'ember-qunit';
import sinon from 'sinon';
import RSVP from 'rsvp';
import ICredentials from 'on-boarding/interfaces/credentials';
import IApiService from 'on-boarding/interfaces/api-service';
import RegisterResponseType from 'on-boarding/types/register-response';

moduleFor('service:register-command-repo', 'Unit | Service | register command repo');

interface FetchResponse extends Response {
  status: number,
  json: () => Promise<RegisterResponseType>
}

test('posts registration data', function(assert: any) {
  const done: Function = assert.async();

  sinon.stub(window, 'fetch').callsFake(function (url: string, options: RequestInit): Promise<FetchResponse> {
    return RSVP.resolve({
      status: 201,
      json: (): RSVP.Promise => {
        const resp: RegisterResponseType = { success: true };
        return RSVP.resolve(resp);
      }
    });
  });
  const data = new FormData();
  data.append('username', 'me');
  data.append('password', 'secret');

  const service: IApiService = this.subject();
  service.post(data).then((resp: FetchResponse) => {
    resp.json().then((resp: RegisterResponseType): void => {
      assert.ok(resp.success);
      const stub: sinon.Stub = window.fetch as sinon.Stub;
      assert.ok(stub.calledOnce);
      const options: RequestInit = {
        method: 'POST',
        body: data
      };
      assert.ok(stub.calledWith('/api/register-command', options);
      stub.restore();
      done();
    });
  });
});

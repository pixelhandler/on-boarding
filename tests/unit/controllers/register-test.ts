import { moduleFor, test } from 'ember-qunit';
import ICredentials from 'on-boarding/interfaces/credentials';
import IRegisterModel from 'on-boarding/interfaces/register-model';
import RegisterModel from 'on-boarding/models/register';

moduleFor('controller:register', 'Unit | Controller | register');

test('update action clones the model', function(assert) {
  const props: ICredentials = {
    username: 'pixelhandler',
    password: 'secret',
    confirmPassword: '',
  };
  const model: IRegisterModel = RegisterModel.create(props);
  let controller = this.subject({model: model});
  assert.ok(controller);
  const currentModel = controller.get('model');
  assert.strictEqual(currentModel, model);

  controller.send('update', model.input.confirmPassword, 'secret');
  const newModel = controller.get('model');
  assert.notStrictEqual(newModel, model);

  const confirmed = controller.get('model.input.confirmPassword.value');
  assert.equal(confirmed, 'secret');
});

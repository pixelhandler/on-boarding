/* global currentURL */
import { test } from 'qunit';
import moduleForAcceptance from 'on-boarding/tests/helpers/module-for-acceptance';
import { visit, find, findAll, fillIn, triggerEvent, click, waitUntil } from 'ember-native-dom-helpers';
import RegisterResponseType from 'on-boarding/types/register-response';
import sinon from 'sinon';
import RSVP from 'rsvp';

moduleForAcceptance('Acceptance | register');

test('visiting /register', async function (assert: any) {
  await visit('/register');
  assert.equal(currentURL(), '/register');
  assert.ok(find('[data-test-id="register"]'));

  const inputs: NodeList = findAll('input');
  assert.equal(inputs.length, 3);

  const labels: NodeList = findAll('label');
  assert.equal(labels.length, 3);

  const button: HTMLButtonElement = find('button[type="submit"]');
  assert.ok(button);
});

interface FetchResponse extends Response {
  status: number,
  json: () => Promise<RegisterResponseType>
}

test('submit form', async function (assert: any) {
  sinon.stub(window, 'fetch').callsFake(function (url: string, options: RequestInit): Promise<FetchResponse> {
    return RSVP.resolve({
      status: 201,
      json: (): RSVP.Promise => {
        const resp: RegisterResponseType = { success: true };
        return RSVP.resolve(resp);
      }
    });
  });

  await visit('/register');

  let selector = 'input[name="username"]';
  await fillIn(selector, 'pixelhandler');
  triggerEvent(selector, 'focusout');

  selector = 'input[name="password"]';
  await fillIn(selector, 'secret');
  triggerEvent(selector, 'focusout');

  selector = 'input[name="confirmPassword"]';
  await fillIn(selector, 'secret');
  triggerEvent(selector, 'focusout');

  selector = 'button[type="submit"]'
  await click(selector);

  await waitUntil(() => find('[data-test-id="success"]'));
  assert.equal(currentURL(), '/success');

  const stub: sinon.Stub = window.fetch as sinon.Stub;
  stub.restore();
});

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import sinon from 'sinon';
import { find, fillIn, triggerEvent } from 'ember-native-dom-helpers';
import InputUpdateActionType from 'on-boarding/types/input-update-action';

moduleForComponent('input-text', 'Integration | Component | input text', {
  integration: true
});

test('default type is text', function (assert) {
  this.set('model', {
    value: 'Wyatt Earp'
  });

  this.render(hbs`{{input-text model=model}}`);

  const el: HTMLInputElement = find('input');
  assert.ok(el);
  assert.equal(el.type, 'text');
  assert.equal(el.value, 'Wyatt Earp');
});

test('can be used for a password input', async function (assert: any) {
  this.set('model', {
    type: 'password',
    value: '',
    maxlength: 16,
    minlength: 8,
  });

  this.render(hbs`{{input-text model=model}}`);

  const el: HTMLInputElement = find('input');
  assert.ok(el);
  assert.equal(el.type, 'password');
  assert.equal(el.value.length, 0);
  assert.equal(el.attributes.maxlength.nodeValue, 16);
  assert.equal(el.attributes.minlength.nodeValue, 8);

  await fillIn('input', 'secret');

  assert.ok(find('input').value.length, 6);
});

test('sends value on focusout event', async function (assert: any) {
  this.set('model', {
    value: ''
  });
  const onUpdate: sinon.Spy = sinon.spy();
  this.set('update', <InputUpdateActionType>onUpdate);

  this.render(hbs`{{input-text model=model onUpdate=update}}`);

  await fillIn('input', 'Doc Holiday');
  const el: HTMLInputElement = find('input');
  assert.equal(el.value, 'Doc Holiday');

  await triggerEvent('input', 'focusout');
  assert.ok(onUpdate.calledOnce);
  assert.ok(onUpdate.calledWith('Doc Holiday'));
});

test('sends value on change event', async function (assert: any) {
  this.set('model', {
    value: ''
  });
  const onUpdate = sinon.spy();
  this.set('update', <InputUpdateActionType>onUpdate);

  this.render(hbs`{{input-text model=model onUpdate=update}}`);

  await fillIn('input', 'Dodge City');
  const el: HTMLInputElement = find('input');
  assert.equal(el.value, 'Dodge City');

  assert.ok(onUpdate.calledOnce);
  assert.ok(onUpdate.calledWith('Dodge City'));
});

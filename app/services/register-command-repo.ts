import Ember from 'ember';
import IApiService from '../interfaces/api-service';
import RegisterResponse from '../types/register-response';

const RegisterCommandService: IApiService = {
  post: function (data: FormData): Promise<Response> {
    const options: RequestInit = {
      method: 'POST',
      body: data
    };
    return window.fetch('/api/register-command', options);
  }
};

export default Ember.Service.extend(RegisterCommandService);

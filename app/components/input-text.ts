import Ember from 'ember';
import IInputTextComponent from '../interfaces/input-text-component';
import InputUpdateAction from '../types/input-update-action';
import InputTextModel from '../models/input-text';
import IInputElement from '../interfaces/input-element';

const { get, getProperties, set, setProperties, computed, computed: {readOnly}, Component } = Ember;

const attrs: Array<string> = ['name', 'labelText', 'tabindex', 'value', 'type', 'minlength', 'maxlength'];

const InputText: IInputTextComponent = {
  tagName: 'input-text',
  classNames: ['m-y(3)'],

  /**
   * @property model
   * @type InputTextModel
   * @required
   */
  model: <InputTextModel>null,

  /**
   * @property onUpdate
   * @type InputUpdateAction
   */
  onUpdate: <InputUpdateAction> function (v: string) {},

  didReceiveAttrs() {
    const model: InputTextModel = get(this, 'model');
    const props: IInputElement = getProperties(model, ...attrs);
    setProperties(this, props);
  },

  name: '',
  type: 'text',
  value: '',
  minlength: <number>null,
  maxlength: <number>null,
  labelText: '',
  tabindex: '0',

  didInsertElement() {
    if (get(this, 'tabindex') === '1') {
      this.element.querySelector('input').focus();
    }
  },

  update(evt: Event): void {
    const target: EventTarget = evt.target;
    const input: HTMLInputElement = evt.target as HTMLInputElement;
    const value: string = input.value;
    const onUpdate: InputUpdateAction = get(this, 'onUpdate');
    onUpdate(value);
  },
};

export default Component.extend(InputText);

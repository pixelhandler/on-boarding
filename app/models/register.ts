import ICredentials from '../interfaces/credentials';
import IInputTextModel from '../interfaces/input-text-model';
import InputTextModel from './input-text';
import IRegisterInputs from '../interfaces/register-inputs';
import IRegisterModel from '../interfaces/register-model';
import freeze from 'on-boarding/utils/freeze';

const _uuids: WeakMap<IRegisterModel, PropertyDecorator|any> = new WeakMap();
const _clones: WeakMap<RegisterModel, PropertyDecorator|any> = new WeakMap();

class RegisterModel implements IRegisterModel {
  input: IRegisterInputs;
  inputs: Array<IInputTextModel>;
  _username: string;
  _password: string;
  _confirmPassword: string;

  constructor(props: ICredentials, clone?: RegisterModel) {
    if (clone) {
      Object.keys(props).forEach(prop => this[`_${prop}`] = props[prop]);
      _clones.set(this, clone);
    } else {
      this._username = isBlank(props.username) ? '' : props.username;
      this._password = isBlank(props.password) ? '' : props.password;
      this._confirmPassword = isBlank(props.confirmPassword) ? '' : props.confirmPassword;
    }
    this.input = {
      username: RegisterModel.inputFactory('username', props, clone),
      password: RegisterModel.inputFactory('password', props, clone),
      confirmPassword: RegisterModel.inputFactory('confirmPassword', props, clone),
    };
    this.inputs = [this.input.username, this.input.password, this.input.confirmPassword];
    freeze(this);
    return this;
  }

  get username(): string { return this._lookup('username'); }
  get password(): string { return this._lookup('password'); }
  get confirmPassword(): string { return this._lookup('confirmPassword'); }

  toJSON(): ICredentials {
    return {
      username: this.username,
      password: this.password,
    };
  }

  clone(props: ICredentials): RegisterModel {
    return RegisterModel.create(props, this);
  }

  static create(props: ICredentials, instance?: RegisterModel): RegisterModel {
    return new RegisterModel(props, instance);
  }

  static inputFactory(name: string, props: ICredentials, clone?: IRegisterModel): InputTextModel {
    let _props: IInputTextModel = { name: name, value: props[name] } as any;
    if (!isBlank(props[name])) {
      _props.value = props[name];
    } else if (clone && !_props.value) {
      _props.value = clone.input[name].value;
    }
    switch (name) {
      case 'username':
        _props.name = 'username';
        _props.labelText = 'Username';
        _props.tabindex = '1';
        break;
      case 'password':
        _props.name = 'password';
        _props.labelText = 'Password';
        _props.type = 'password';
        _props.tabindex = '2';
        break;
      case 'confirmPassword':
        _props.name = 'confirmPassword';
        _props.labelText = 'Confirm Password';
        _props.type = 'password';
        _props.tabindex = '3';
      default:
        break;
    }
    return new InputTextModel(_props, clone ? clone.input[name] : null);
  }

  private _lookup(prop: string) {
    let value;
    const _prop = `_${prop}`;
    if (Object.prototype.hasOwnProperty.call(this, _prop)) {
      value = this[_prop];
    } else {
      let clone = _clones.get(this);
      if (clone && Object.prototype.hasOwnProperty.call(clone, _prop)) {
        value = clone[_prop];
      } else if (clone) {
        return this._lookup.call(clone, prop);
      }
    }
    return value;
  }

  // Ember needs to set a uuid
  __defineNonEnumerable(property) {
    _uuids.set(this, property);
  }
}

function isBlank(value: any): boolean {
  return value === null || typeof value === 'undefined';
}

export default RegisterModel;

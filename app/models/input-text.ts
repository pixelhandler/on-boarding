import IInputElement from '../interfaces/input-element';
import IInputTextModel from '../interfaces/input-text-model';
import freeze from 'on-boarding/utils/freeze';

const _uuids: WeakMap<IInputTextModel, PropertyDecorator|any> = new WeakMap();
const _clones: WeakMap<InputTextModel, PropertyDecorator|any> = new WeakMap();

class InputTextModel implements IInputTextModel {
  _name: string;
  _type: string;
  _value: string;
  _labelText: string;
  _tabindex: string;
  _maxlength: number;
  _minlength: number;

  constructor(props: IInputElement, clone?: InputTextModel) {
    if (clone) {
      Object.keys(props).forEach(prop => this[`_${prop}`] = props[prop]);
      _clones.set(this, clone);
    } else {
      this._name = isBlank(props.name) ? '' : props.name;
      this._type = isBlank(props.type) ? 'text' : props.type;
      this._value = isBlank(props.value) ? '' : props.value;
      this._maxlength = isBlank(props.maxlength) ? 80 : props.maxlength;
      this._minlength = isBlank(props.minlength) ? 0 : props.minlength;
      this._labelText = isBlank(props.labelText) ? '' : props.labelText;
      this._tabindex = isBlank(props.tabindex) ? '' : props.tabindex;
    }
    freeze(this);
    return this;
  }

  get name(): string { return this._lookup('name'); }
  get type(): string { return this._lookup('type'); }
  get value(): string { return this._lookup('value'); }
  get maxlength(): number { return this._lookup('maxlength'); }
  get minlength(): number { return this._lookup('minlength'); }
  get labelText(): string { return this._lookup('labelText'); }
  get tabindex(): string { return this._lookup('tabindex'); }

  static create(props: IInputElement, instance?: InputTextModel): InputTextModel {
    return new InputTextModel(props, instance);
  }

  private _lookup(prop: string) {
    let value;
    const _prop = `_${prop}`;
    if (Object.prototype.hasOwnProperty.call(this, _prop)) {
      value = this[_prop];
    } else {
      let clone = _clones.get(this);
      if (clone && Object.prototype.hasOwnProperty.call(clone, _prop)) {
        value = clone[_prop];
      } else if (clone) {
        return this._lookup.call(clone, prop);
      }
    }
    return value;
  }

  // Ember needs to set a uuid
  __defineNonEnumerable(property) {
    _uuids.set(this, property);
  }
}

function isBlank(value: any): boolean {
  return value === null || typeof value === 'undefined';
}

export default InputTextModel;

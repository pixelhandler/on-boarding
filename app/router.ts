import Ember from 'ember';
import config from './config/environment';

const Router: any|Ember.Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootUrl
});

Router.map(function(): void {
  this.route('register');
  this.route('success');
});

export default Router;

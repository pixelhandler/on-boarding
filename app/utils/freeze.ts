import config from '../config/environment';

export default function freeze(item: Object | Array<any>) {
  if (config.environment !== 'production') {
    if (isObject(item)) {
      Object.keys(item).forEach(k => {
        const prop: any = item[k];
        if (isNotPrimitive(prop)) {
          freeze(prop);
        }
      });
    } else if (isArray(item)) {
      const list = item as Array<any>;
      list.forEach((i: any) => {
        if (isNotPrimitive(i)) {
          freeze(i);
        }
      });
    }
    if (!Object.isFrozen(item)) {
      Object.freeze(item);
    }
  }
  return item;
}

function isNotPrimitive(i) {
  return isObject(i) || isArray(i);
}

function isObject(o: Object | any) {
  return Object.prototype.toString.call(o) === '[object Object]';
}

function isArray(a: Array<any> | any) {
  return Object.prototype.toString.call(a) === '[object Array]';
}
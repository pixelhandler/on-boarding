type InputUpdateAction = (v: string) => void;

export default InputUpdateAction;

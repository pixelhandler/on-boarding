type RegisterResponse = {
  success: boolean
}

export default RegisterResponse;

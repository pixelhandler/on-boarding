import Ember from 'ember';
import ICredentials from '../interfaces/credentials';
import IInputElement from '../interfaces/input-element';

const { set, isEmpty, Controller } = Ember;

interface IRegisterController {
  actions: {
    register: () => void,
    update: (input: IInputElement, value: string) => void,
  }
}

const RegisterController: IRegisterController = {
  actions: {
    register(): void {
      const register: HTMLFormElement = document.forms.register;
      const data = new FormData(register);
      const username = data.get('username');
      const password = data.get('password');
      const confirmed = data.get('confirmPassword');
      if (isEmpty(username)) {
        alert('Please enter your username');
      } else if (isEmpty(password) || password !== confirmed) {
        alert('Please confirm your password');
      } else {
        data.delete('confirmPassword');
        this.send('submit', data);
      }
    },

    update(input: IInputElement, value: string): void {
      const props: ICredentials = {};
      props[input.name] = value;
      const model: IInputElement = this.model.clone(props);
      console.log(model);
      set(this, 'model', model);
    }
  }
}

export default Controller.extend(RegisterController);

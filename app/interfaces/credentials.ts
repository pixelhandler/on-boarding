interface ICredentials {
  username?: string;
  password?: string;
  confirmPassword?: string;
}

export default ICredentials;

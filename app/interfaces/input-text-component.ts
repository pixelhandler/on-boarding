import InputTextModel from '../models/input-text';
import IInputElement from '../interfaces/input-element';

interface IInputTextComponent extends IInputElement {
  model: InputTextModel,
  tagName: string,
  classNames: Array<string>,
  didReceiveAttrs(): void;
  didInsertElement(): void;
  onUpdate: (v: string) => void,
  update: (e: Event) => void,
}

export default IInputTextComponent;

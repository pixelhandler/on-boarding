import IInputElement from '../interfaces/input-element';

interface IInputTextModel extends IInputElement {
  _name: string;
  _type: string;
  _value: string;
  _labelText: string;
  _tabindex: string;
  _maxlength?: number;
  _minlength?: number;
}

export default IInputTextModel;

import iCredentials from './credentials';

interface IApiService {
  post: (data: FormData) => Promise<Response>;
}

export default IApiService;

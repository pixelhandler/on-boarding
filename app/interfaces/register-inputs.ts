import IInputTextModel from './input-text-model';

interface IRegisterInputs {
  username: IInputTextModel,
  password: IInputTextModel,
  confirmPassword: IInputTextModel,
}

export default IRegisterInputs;

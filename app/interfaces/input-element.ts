interface IInputElement {
  name?: string,
  value?: string,
  labelText?: string,
  tabindex?: string;
  type?: string,
  minlength?: number,
  maxlength?: number,
}

export default IInputElement;

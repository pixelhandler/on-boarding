import ICredentials from './credentials';
import IInputTextModel from './input-text-model';
import IRegisterInputs from './register-inputs';

interface IRegisterModel extends ICredentials {
  input: IRegisterInputs;
  inputs: Array<IInputTextModel>;
  confirmPassword: string;
  _username: string;
  _password: string;
  _confirmPassword: string;
}

export default IRegisterModel;

import Ember from 'ember';
import ICredentials from '../interfaces/credentials';
import IRegisterModel from '../interfaces/register-model';
import RegisterModel from '../models/register';
import RSVP from 'rsvp';

const { get, inject: { service }, Route } = Ember;

interface IRegisterRoute {
  repo: Ember.Service,
  model(params: {}, transition: EmberStates.Transition): RSVP.Promise<IRegisterModel>;
  actions: {
    submit(data: FormData): void;
  }
}

const RegisterRoute: IRegisterRoute = {
  repo: service('register-command-repo'),
  model(params: {}, transition: EmberStates.Transition): RSVP.Promise<IRegisterModel> {
    const props: ICredentials = {};
    const model: IRegisterModel = RegisterModel.create(props);
    return RSVP.resolve(model);
  },
  actions: {
    submit(data: FormData): void {
      get(this, 'repo').post(data).then(resp => {
        if (resp.status === 201) {
          this.transitionTo('success');
        } else {
          alert('Please try again!')
        }
      })
    }
  }
};

export default Route.extend(RegisterRoute);

import Ember from 'ember';
import RSVP from 'rsvp';

const { Route } = Ember;

interface ISuccessRoute {
  model(params: {}, transition: EmberStates.Transition): RSVP.Promise<{}>;
}

const SuccessRoute: ISuccessRoute = {
  model(params: {}, transition: EmberStates.Transition) {
    return RSVP.resolve({ message: 'Thanks, your registration was sent.' });
  },
};

export default Route.extend(SuccessRoute);
